package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class BolaFuego {
	//VARIABLES DE INSTANCIA
	private int x;
	private int y;
	private int alto;
	private int ancho;
	private int velocidad;
	private Image bola;
	
	//CONSTRUCTOR
	BolaFuego(int x, int y, int alto, int ancho){
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		this.velocidad = 5;
		this.bola = Herramientas.cargarImagen("Bola.png");
	}
	
	//METODOS
	
	public void mover() {
		this.x = this.x + velocidad;		
	}

	
	public void dibujarBola(Entorno e) {
		e.dibujarImagen(this.bola, this.x, this.y, 0, 0.1);	
	}
	
	//GETTERS 
	
	public int getX() {
		return x;
	}	
	public int getAncho() {
		return ancho;
	}
	public int getAlto() {
		return alto;
	}
	public int getY() {
		return y;
	}

}