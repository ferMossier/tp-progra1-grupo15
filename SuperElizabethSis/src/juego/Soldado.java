package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Soldado {
	//VARIABLES DE INSTANCIA
	private int x;
	private int y;
	private int alto;
	private int ancho;
	private int velocidad;
	private Image soldado;
	
	//CONSTRUCTOR
	Soldado(int x, int y, int alto, int ancho, int velocidad){
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		this.velocidad = velocidad;
		this.soldado = Herramientas.cargarImagen("Soldado.png");
	}
	
	//METODOS 
	
	public void mover() {
		this.x = this.x - velocidad;
	}
		
	public void dibujarSoldado(Entorno e) {
		e.dibujarImagen(this.soldado, this.x, this.y, 0, 0.2);
	}
	
	//GETTERS Y SETTERS
	
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	public void setX(int x) {
		this.x = x;
	}
	public void setY(int y) {
		this.y = y;
	}	
	public int getAncho() {
		return ancho;
	}
	public int getAlto() {
		return alto;
	}
	public void setVelocidad(int velocidad) {
		this.velocidad = velocidad;
	}	

}
