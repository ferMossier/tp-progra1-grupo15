package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Princesa {
	//VARIABLES DE INSTANCIA
	private int x;
	private int y;
	private int alto;
	private int ancho;
	private int velocidad;
	private boolean saltando;
	private Image princesa;
	
	//CONSTRUCTOR
	Princesa(int x, int y, int ancho, int alto){
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		this.velocidad = 2;
		this.saltando = false;
		this.princesa = Herramientas.cargarImagen("Princesa.png");
	}
	
	//METODOS
	
	public void dibujarPrincesa(Entorno e) {
		e.dibujarImagen(this.princesa, this.x, this.y, 0, 0.3);
	}
		
	public boolean isSaltando() {
		return saltando;
	}
	
	public void desplazarIzquierda() {
		this.x = this.x - velocidad;				
	}
	
	public void desplazarDerecha() {
		this.x = this.x + velocidad;
	}
	
	public void subir() {
		this.y = this.y - velocidad-1;
	}
	
	public void bajar() {
		this.y = this.y + velocidad;
	}
	
	public void setSaltando(boolean saltando) {
		this.saltando = saltando;
	}
	
	//GETTERS Y SETTERS
	
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	public int getAlto() {
		return alto;
	}
	public int getAncho() {
		return ancho;
	}
	public int getVelocidad() {
		return velocidad;
	}	
}
