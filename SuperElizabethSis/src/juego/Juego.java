package juego;
import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego
{
	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	private Princesa elis;
	private Obstaculo arrayobs [];
	private Obstaculo obs;
	private BolaFuego bola;
	private Soldado soldier;
	private Soldado arraySoldier [];
	private int vidas;
	private int min;
	private int max;
	private int velocidad;
	private Image fondo;
	
	Juego()
	{
		this.entorno = new Entorno(this, "Super Elizabeth Sis - Grupo 15 - v1: Giannini Leila, Mossier Fernando, Oberosler Ayrton", 800, 600);		
		this.elis = new Princesa(30, 450, 20, 70);
		this.vidas = 3;
		this.min = entorno.ancho();
		this.max = entorno.ancho()*2;
		this.velocidad = 1;
		this.fondo = Herramientas.cargarImagen("fondo.png");
		
		crearObstaculos();
		crearSoldados();
				
		this.entorno.iniciar();
	}

	//OTORGA NUMEROS ENTEROS RANDOM ENTRE UN MINIMO Y UN MAXIMO DADO
	public int getRandom(int min, int max) {
		//Me da numeros random entre min y max
		int x = (int)(Math.random()*((max - min)+1))+min;
		return x;
	}
		
	//OTORGA UN ARRAY ORDENADO DE RANDOMS CON SEPARACION MINIMA DE 70 Y MAXIMA DE 150.
	public int[] getRandomArray(int pos) {
		int[] randomArray = new int[pos];
		randomArray[0] = getRandom(this.min, this.min+100);	//Numero random entre 800 y 900	
		for(int i = 1; i<randomArray.length; i++) {
			int distancia = getRandom(70, 250);
			randomArray[i] = randomArray[i-1]+distancia;
		}		
		return randomArray;
	}
	//INDICA EL MAYOR VALOR DE UN ARRAY DE ENTEROS
	public int mayor(int[] lista) {
		int mayor = lista[0];
		for(int i=0;i<lista.length;i++) {
			if(lista[i] > mayor) {
				mayor = lista[i];
			}
		}
		return mayor;
	}
	
	public void dibujarFondo(Entorno e) {
		e.dibujarImagen(this.fondo, 400, 300, 0, 1); //dibuja la imagen de fondo, inicializada en el constructor.
	}
		
	//CREAR ARRAY DE OBSTACULOS
	public void crearObstaculos() {
		this.arrayobs=new Obstaculo [5];
		int [] arrayXObs = {800, 950, 1110, 1300, 1420}; //Array de posiciones fijas en x de los obstaculos
		for (int i=0; i<this.arrayobs.length;i++) {
			this.arrayobs [i] = new Obstaculo(arrayXObs[i], entorno.alto()-50, 50, 20, this.velocidad);
			}
	}
	//CREAR ARRAY DE SOLDADOS	
	public void crearSoldados() {
		this.arraySoldier=new Soldado [4];
		int [] arrayXSold = getRandomArray(4);
		for (int i=0; i<arraySoldier.length;i++) {
			this.arraySoldier[i] = new Soldado(arrayXSold[i], 550, 70, 20, this.velocidad + 1);
		}
	}
	
	//COLISION PRINCESA	OBSTACULO
	public boolean colisionPrincesaObstaculo() {
		int ladoIPrincesa=this.elis.getX()- elis.getAncho()/2;
		int ladoDerPrincesa=this.elis.getX()+ elis.getAncho()/2;
		int basePrincesa=this.elis.getY()+ this.elis.getAlto()/2;
		
		for (int i = 0; i < arrayobs.length; i++) {
			int superficieObs=this.arrayobs[i].getY()-this.arrayobs[i].getAlto()/2;
			int ladoIObst=this.arrayobs[i].getX()-this.arrayobs[i].getAncho()/2;
			int ladoDerObst=this.arrayobs[i].getX()+this.arrayobs[i].getAncho()/2;			
			//colision desde arriba
			boolean seTocanY= basePrincesa > superficieObs;
			boolean seTocanX= (ladoIPrincesa > ladoIObst && ladoIPrincesa < ladoDerObst) || (ladoDerPrincesa < ladoDerObst && ladoDerPrincesa > ladoIObst);
			
			if (seTocanY && seTocanX) {
				return true;
			}
		}
		return false;
	}
	
	//COLISION PRINCESA SOLDADO
	public boolean colisionPrincesaSoldado() {
		int ladoIPrincesa=this.elis.getX()- elis.getAncho()/2;
		int ladoDerPrincesa=this.elis.getX()+ elis.getAncho()/2;
		int basePrincesa=this.elis.getY()+ this.elis.getAlto()/2;
		
		for (int i = 0; i < arraySoldier.length; i++) {
			int superficieSold=this.arraySoldier[i].getY()-this.arraySoldier[i].getAlto()/2;
			int ladoISold=this.arraySoldier[i].getX()-this.arraySoldier[i].getAncho()/2;
			int ladoDerSold=this.arraySoldier[i].getX()+this.arraySoldier[i].getAncho()/2;			
			//colision desde arriba
			boolean seTocanY= basePrincesa > superficieSold;
			boolean seTocanX= (ladoIPrincesa > ladoISold && ladoIPrincesa < ladoDerSold) || (ladoDerPrincesa < ladoDerSold && ladoDerPrincesa > ladoISold);

			if (seTocanY && seTocanX) {
				return true;
			}
		}
		return false;
	}
	
	public void reiniciaPosicion() {
		this.elis = new Princesa(30, 450, 20, 70);
		crearObstaculos();
		crearSoldados();
		borrarBola();
	}
	
	//COLISION BOLA SOLDADO
	public boolean colisionBolaSoldado() {
		if(this.bola != null && this.soldier != null && this.bola.getX()+this.bola.getAncho()/2>=this.soldier.getX()-this.soldier.getAncho()/2 && this.bola.getX() <= this.soldier.getX()+this.soldier.getAncho()/2 && this.bola.getY()+this.bola.getAlto()/2>this.soldier.getY()-this.soldier.getAlto()/2) {
			return true;
		}
	    return false;	
	}
	
	//BORRAR BOLA DE FUEGO
	public void borrarBola() {
		this.bola = null;
	}
	
	int contador = 0;
	int puntos = 0;
	boolean pause = false;

	public void tick()
	{
		
		if(entorno.sePresiono('p')) {
			pause = !pause;			
		}
		dibujarFondo(this.entorno);
		
		if(pause == false) {
			//DIBUJAR E INTERACTUAR PRINCESA
			this.elis.dibujarPrincesa(this.entorno);
			if(this.entorno.estaPresionada(this.entorno.TECLA_DERECHA) && elis.getX() < this.entorno.ancho()-400) {
				elis.desplazarDerecha();
			}
			if(this.entorno.estaPresionada(this.entorno.TECLA_IZQUIERDA) && elis.getX() > 25) {
				elis.desplazarIzquierda();
			}			
			if(this.entorno.sePresiono(this.entorno.TECLA_ARRIBA) && elis.getY() > 500 + elis.getAncho()/2) {
				elis.setSaltando(true);
			}
			if (elis.isSaltando()) {
				elis.subir();
			}
			if (!elis.isSaltando()&& elis.getY()<550)
				elis.bajar();
			if(elis.getY()<420) {
				elis.setSaltando(false);				
			}
						
			if (colisionPrincesaObstaculo() && this.vidas >0) {
				reiniciaPosicion();
				this.vidas--;			
			}
			if(colisionPrincesaSoldado() && this.vidas > 0) {
				reiniciaPosicion();
				this.vidas--;
			}
			if (vidas==0){
				entorno.escribirTexto("G A M E  O V E R", 370, 300);
				entorno.escribirTexto("P U N T A J E : " + Integer.toString(puntos), 370, 350);
			}
			if(puntos >= 0 && puntos < 100) {
				this.entorno.escribirTexto("N I V E L   1  ", 400,70);
			}
			else if(puntos >= 100) {
				this.entorno.escribirTexto("N I V E L   2  ", 400,70);
			}
			this.entorno.escribirTexto("VIDAS  " + Integer.toString(vidas), 700,70);
			this.entorno.escribirTexto("PUNTOS  " + Integer.toString(puntos), 700,100);
						
			//DIBUJAR OBSTACULOS
			if(vidas > 0) {
				for (int i=0; i<this.arrayobs.length; i++) {
					this.obs = this.arrayobs[i];
		            this.obs.dibujarObstaculo(entorno);
		            if(puntos >= 100) {
		            	this.arrayobs[i].setVelocidad(this.velocidad+1);
		            }
		            this.obs.mover();           
		            if(obs.getX() <= -20) {
		                this.obs.setX(entorno.ancho()+20); //cuando desaparecen del lado izquierdo de la pantalla, reaparecen del lado derecho
		            
		            }
				}
			}
						
			//DIBUJAR SOLDADOS
			if(vidas > 0) {
				for (int i=0; i<this.arraySoldier.length; i++) {
					this.soldier = this.arraySoldier[i];
					this.soldier.dibujarSoldado(entorno);
					if(puntos >= 100) {
		            	this.arraySoldier[i].setVelocidad(this.velocidad+2);
		            }
					this.soldier.mover();
			        //COLISION BOLA SOLDADO
					int[] posiciones = new int[arraySoldier.length];
					posiciones[0] = arraySoldier[0].getX();
					posiciones[1] = arraySoldier[1].getX();
					posiciones[2] = arraySoldier[2].getX();
					posiciones[3] = arraySoldier[3].getX();
					if(this.soldier != null && this.soldier.getX() <= 0) {	
						int x = getRandom(800, 1000);
						this.arraySoldier[i] = new Soldado(mayor(posiciones) + x, entorno.alto()-50, 70, 20, this.velocidad + 1); //Crea un nuevo soldado en una posicion X aleatoria mayor a la posicion del soldado mas lejano
					}
					if(colisionBolaSoldado()) {
						borrarBola();
						int x = getRandom(300, 400);
						this.arraySoldier[i] = new Soldado(mayor(posiciones) + x, entorno.alto()-50, 70, 20, this.velocidad + 1); //Crea un nuevo soldado en una posicion X aleatoria mayor a la posicion del soldado mas lejano
						puntos = puntos +5;
					}
				}
			}
						
			//DIBUJAR BOLA DE FUEGO
			if(vidas > 0) {
				if(this.entorno.sePresiono(this.entorno.TECLA_ESPACIO) && contador == 0){
					this.bola = new BolaFuego(this.elis.getX(), this.elis.getY(), 30, 20);
					contador = contador + 1;
					//Este contador sirve para que solo se pueda disparar de a una bola de fuego por vez.
				}				
				if(this.bola != null && this.bola.getX() < entorno.ancho()) {
					this.bola.dibujarBola(this.entorno);
					this.bola.mover();
				}else {
					contador = 0;
					this.bola = null;
				}
			}
		}
		else {
			entorno.escribirTexto("P A U S E ", 370, 300);
		}			
	}

	@SuppressWarnings("unused")
	public static void main(String[] args)
	{
		Juego juego = new Juego();
	}
}
